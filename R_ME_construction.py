# %%
from distutils.errors import CompileError
import pandas as pd
import numpy as np
import datetime as dt
from pyparsing import col
import wrds
import matplotlib.pyplot as plt
# from dateutil.relativedelta import *
from pandas.tseries.offsets import MonthEnd, YearEnd
from scipy import stats
# %%
conn=wrds.Connection()
# %%
sample_start_date = '01/01/2010'
sample_end_date = '12/31/2021'
# %%
# CRSP
crsp_m = conn.raw_sql(f"""
                       select a.permno, a.permco, a.date, b.siccd, b.shrcd, 
                       b.exchcd, a.ret, a.retx, a.shrout, a.prc
                       from crsp.msf as a
                       left join crsp.msenames as b
                       on a.permno=b.permno
                       and b.namedt <= a.date
                       and a.date <= b.nameendt
                       where a.date between '{sample_start_date}' and '{sample_end_date}'
                       and b.exchcd between 1 and 3
                       """, date_cols=['date'])
# %%
# filter out financial firms sic 6000 to 6999
crsp_m = crsp_m[(crsp_m['siccd']<6000) | (crsp_m['siccd']>6999)]

# crsp date convention is different from compustat, need to line up
crsp_m['jdate'] = crsp_m['date']+MonthEnd(0)
# %%
# adjust for delisting return
dlret = conn.raw_sql("""
                     select permno, dlret, dlstdt
                     from crsp.msedelist
                     """, date_cols=['dlstdt'])

dlret['jdate'] = dlret['dlstdt']+MonthEnd(0)
#%%
crsp = pd.merge(crsp_m, dlret, how='left', on=['permno', 'jdate'])
crsp['dlret']=crsp['dlret'].fillna(0)
crsp['ret']=crsp['ret'].fillna(0)

# return adjustment
crsp['retadj']=(1+crsp['ret'])*(1+crsp['dlret'])-1
# %%
# market equity
crsp['me']=crsp['prc'].abs()*crsp['shrout']
crsp=crsp.drop(['dlret', 'dlstdt', 'prc', 'shrout'], axis=1)
crsp=crsp.sort_values(by=['jdate', 'permco', 'me'])

# %%
# Aggregate all Market Equity from all stocks issued by the same company 

# sum of me across different permno belonging to the same permco at a given date
crsp_summe = crsp.groupby(['jdate', 'permco'])['me'].sum().reset_index()
# largest market cap within a permco and date
crsp_maxme = crsp.groupby(['jdate', 'permco'])['me'].max().reset_index()

# join by jdate and maxme to find the permno that has the maximum me
crsp1 = pd.merge(crsp, crsp_maxme, how='inner', on=['jdate', 'permco', 'me'])
crsp1 = crsp1.drop(['me'], axis=1)

# merge with sum of me to get the aggregate market equity for each company
crsp2 = pd.merge(crsp1, crsp_summe, how='inner', on=['jdate', 'permco'])
crsp2 = crsp2.sort_values(by=['permno', 'jdate']).drop_duplicates()
# %%
crsp2['year'] = crsp2['jdate'].dt.year
crsp2['month'] = crsp2['jdate'].dt.month
# decme = crsp2[crsp2['month']==12]
# decme=decme[['permno', 'date', 'jdate', 'me', 'year']].rename(columns={'me':'dec_me'})
# %%
# July to June dates

# for January to June in year t+1, their portfolio classification are
# determined in June year t, we define July at year t to June year t+1
# as a qyear. So we subtract 6 months from calender year to determine 
# their qyear
crsp2['qdate'] = crsp2['jdate']+MonthEnd(-6)
crsp2['qyear'] = crsp2['qdate'].dt.year
crsp2['qmonth'] = crsp2['qdate'].dt.month

# to determine period t portfolio need to use period t-1 characteristic
crsp2['lme'] = crsp2.groupby(['permno'])['me'].shift(1)
# %%
# Info as of June
crsp_jun = crsp2[crsp2['month']==6]
crsp_jun=crsp_jun[['permno','date', 'jdate', 'shrcd','exchcd','retadj','me']]
crsp_jun=crsp_jun.sort_values(by=['permno','jdate']).drop_duplicates()
crsp_jun['count'] = crsp_jun.groupby(['permno']).cumcount()
# %%
nyse = crsp_jun[(crsp_jun['exchcd']==1) & (crsp_jun['me']>0) & (crsp_jun['count']>=1) \
                & ((crsp_jun['shrcd'] == 10) | (crsp_jun['shrcd'] == 11))]

nyse_sz = nyse.groupby(['jdate'])['me'].median().to_frame().reset_index().rename(columns={'me':'sizemedn'})

#%%
crsp1_jun = pd.merge(crsp_jun, nyse_sz, how='left', on=['jdate'])
crsp1_jun = crsp1_jun.dropna()
# %%
def sz_bucket(row):
    if row['me'] == np.nan:
        value = ''
    elif row['me'] <= row['sizemedn']:
        value = 'S'
    elif row['me'] > row['sizemedn']:
        value = 'B'
    return value
# %%
crsp1_jun['szport'] = np.where((crsp1_jun['me'] > 0) & (crsp1_jun['count'] >=1), crsp1_jun.apply(sz_bucket, axis=1), '')
crsp1_jun['nonmissport'] = np.where(crsp1_jun['szport']!='', 1, 0)
crsp1_jun['qyear'] = crsp1_jun['jdate'].dt.year
# %%
crsp3 = crsp2[['date', 'permno', 'shrcd', 'exchcd', 'retadj', 'me', 'lme', 'qyear', 'jdate']]
crsp3 = crsp3.merge(crsp1_jun[['permno', 'qyear', 'szport', 'nonmissport']], how='left', on=['permno', 'qyear'])

# %%
crsp4 = crsp3[(crsp3['lme'] > 0) & (crsp3['nonmissport']==1)\
              & ((crsp3['shrcd'] == 10) | (crsp3['shrcd'] == 11))]
# %%
def wavg(group, retadj, weight):
    d = group[retadj]
    w = group[weight]
    try:
        return (d*w).sum() / w.sum()
    except:
        return np.nan
# %%
vwret = crsp4.groupby(['jdate', 'szport']).apply(wavg, 'retadj', 'lme').to_frame().reset_index().rename(columns={0: 'vwret'})
# %%
qfactor = vwret.pivot(index='jdate', columns='szport', values='vwret').reset_index()
# %%
qfactor['R_ME'] = qfactor['S'] - qfactor['B']
# %%
# Lau's q factor
q5_factor = pd.read_csv('q5_factors_monthly_2021.csv')
q5_factor = q5_factor[q5_factor['year'].astype(str)>=sample_start_date[-4:]].reset_index(drop=True)

# %%
stats.pearsonr(q5_factor.iloc[18:144,:]['R_ME'], qfactor['R_ME'])
# %%
