# %%
from distutils.errors import CompileError
import pandas as pd
import numpy as np
import datetime as dt
from pyparsing import col
import wrds
import matplotlib.pyplot as plt
# from dateutil.relativedelta import *
from pandas.tseries.offsets import MonthEnd, YearEnd
from scipy import stats

# %%
conn=wrds.Connection()
# %%
sample_start_date = '01/01/2010'
sample_end_date = '12/31/2021'

# %%
# ROE = IBQ / 1 quarter lagged book equity
comp = conn.raw_sql(f"""
                     select gvkey, datadate, txditcq, seqq, ceqq, pstkq, atq, ltq, pstkrq,
                     ibq, rdq
                     from comp.fundq
                     where indfmt = 'INDL'
                     and datafmt = 'STD'
                     and popsrc='D'
                     and consol='C'
                     and curcdq='USD'
                     and datadate between '{sample_start_date}' and '{sample_end_date}'
                     """, date_cols=['datadate'])
# %%
# some rows are duplicated and may contain different values, need to drop
comp = comp.loc[comp[['gvkey', 'datadate']].drop_duplicates().index, :]
comp['year'] = comp['datadate'].dt.year
# %%

# compute the book equity

# define shareholder's equity by this order
comp.loc[:, 'se'] = comp['seqq']
comp.loc[:, 'se'] = np.where(comp.loc[:, 'se'].isnull(), comp['ceqq']+comp['pstkq'], comp['se'])
comp.loc[:, 'se'] = np.where(comp.loc[:, 'se'].isnull(), comp['atq']-comp['ltq'], comp['se'])

# %%
comp.loc[:, 'be'] = comp['se'] + comp['txditcq'] - comp['pstkrq']

# Filter company with negative book equity
comp['be'] = np.where(comp['be'] > 0, comp['be'], np.nan)

# %%
# 1 quarter lagged be
comp['lbe'] = comp.groupby(['gvkey'])['be'].shift()

#%%

comp['ROE'] = comp['ibq'] / comp['lbe']

# %%
# drop used columns
comp = comp.drop(columns=['seqq', 'ceqq', 'pstkq', 'atq', 'ltq', 'txditcq', 'pstkrq'])
# %%
#CRSP
crsp_m = conn.raw_sql(f"""
                       select a.permno, a.permco, a.date, b.siccd, b.shrcd, 
                       b.exchcd, a.ret, a.retx, a.shrout, a.prc
                       from crsp.msf as a
                       left join crsp.msenames as b
                       on a.permno=b.permno
                       and b.namedt <= a.date
                       and a.date <= b.nameendt
                       where a.date between '{sample_start_date}' and '{sample_end_date}'
                       and b.exchcd between 1 and 3
                       """, date_cols=['date'])
# %%
# filter out financial firms sic 6000 to 6999
crsp_m = crsp_m[(crsp_m['siccd']<6000) | (crsp_m['siccd']>6999)]

# crsp date convention is different from compustat, need to line up
crsp_m['jdate'] = crsp_m['date']+MonthEnd(0)
# %%
# adjust for delisting return
dlret = conn.raw_sql("""
                     select permno, dlret, dlstdt
                     from crsp.msedelist
                     """, date_cols=['dlstdt'])

dlret['jdate'] = dlret['dlstdt']+MonthEnd(0)
#%%
crsp = pd.merge(crsp_m, dlret, how='left', on=['permno', 'jdate'])
crsp['dlret']=crsp['dlret'].fillna(0)
crsp['ret']=crsp['ret'].fillna(0)

# return adjustment
crsp['retadj']=(1+crsp['ret'])*(1+crsp['dlret'])-1
# %%
# market equity
crsp['me']=crsp['prc'].abs()*crsp['shrout']
crsp=crsp.drop(['dlret', 'dlstdt', 'prc', 'shrout'], axis=1)
crsp=crsp.sort_values(by=['jdate', 'permco', 'me'])
# %%
# Aggregate all Market Equity from all stocks issued by the same company 

# sum of me across different permno belonging to the same permco at a given date
crsp_summe = crsp.groupby(['jdate', 'permco'])['me'].sum().reset_index()
# largest market cap within a permco and date
crsp_maxme = crsp.groupby(['jdate', 'permco'])['me'].max().reset_index()

# join by jdate and maxme to find the permno that has the maximum me
crsp1 = pd.merge(crsp, crsp_maxme, how='inner', on=['jdate', 'permco', 'me'])
crsp1 = crsp1.drop(['me'], axis=1)

# merge with sum of me to get the aggregate market equity for each company
crsp2 = pd.merge(crsp1, crsp_summe, how='inner', on=['jdate', 'permco'])
crsp2 = crsp2.sort_values(by=['permno', 'jdate']).drop_duplicates()
# %%
crsp2['year'] = crsp2['jdate'].dt.year
crsp2['month'] = crsp2['jdate'].dt.month
# %%
# July to June dates
crsp2['qdate'] = crsp2['jdate']+MonthEnd(-6)
crsp2['qyear'] = crsp2['qdate'].dt.year
crsp2['qmonth'] = crsp2['qdate'].dt.month
# %%
# to determine month t portfolio weight need to use month t-1 characteristic
crsp2['lme'] = crsp2.groupby(['permno'])['me'].shift(1)

# %%
# CCM
ccm=conn.raw_sql("""
                  select gvkey, lpermno as permno, linktype, linkprim, 
                  linkdt, linkenddt
                  from crsp.ccmxpf_linktable
                  where substr(linktype,1,1)='L'
                  and (linkprim ='C' or linkprim='P')
                  """, date_cols=['linkdt', 'linkenddt'])

ccm['linkenddt']=ccm['linkenddt'].fillna(pd.to_datetime('today'))

# %%
# ccm merged with comp
ccm1 = pd.merge(comp[['gvkey', 'datadate', 'be', 'ROE']], ccm, how='left', on=['gvkey'])

#%%
ccm1['jdate'] = ccm1['datadate'] + MonthEnd(0)

# %%
# set link date bounds
ccm2=ccm1[(ccm1['jdate']>=ccm1['linkdt'])&(ccm1['jdate']<=ccm1['linkenddt'])]
ccm2=ccm2[['gvkey','permno','datadate', 'jdate', 'be','ROE']]
# %%
ccm3 = crsp2.merge(ccm2, how='left', on=['permno', 'jdate'])
# %%
# forward fiil the ROE value until next quarter's ROE become available
ccm3['ROE'] = ccm3.groupby(['permno'])['ROE'].ffill()
# %%
nyse = ccm3[(ccm3['exchcd']==1) & (ccm3['me']>0) \
                & ((ccm3['shrcd'] == 10) | (ccm3['shrcd'] == 11))]
# %%
nyse_roe = nyse.groupby(['jdate'])['ROE'].describe(percentiles=[0.3, 0.7]).reset_index()
nyse_roe = nyse_roe[['jdate', '30%', '70%']].rename(columns={'30%': 'ROE30', '70%': 'ROE70'})
# %%
ccm4 = ccm3.merge(nyse_roe, how='left', on=['jdate'])
# %%
def ROE_bucket(row):
    if row['ROE'] <= row['ROE30']:
        value = 'L'
    elif row['ROE'] <= row['ROE70']:
        value = 'M'
    elif row['ROE'] > row['ROE70']:
        value = 'H'
    else:
        value =''
    return value
# %%
ccm4['roeport'] = np.where((ccm4['me']>0), ccm4.apply(ROE_bucket, axis=1), '')
ccm4['nonmissport'] = np.where((ccm4['roeport']!=''), 1, 0)
# %%
# keeping only records that meet the criteria
ccm5=ccm4[(ccm4['lme']>0) & (ccm4['nonmissport']==1) & 
          ((ccm4['shrcd']==10) | (ccm4['shrcd']==11))]
# %%
def wavg(group, retadj, weight):
    d = group[retadj]
    w = group[weight]
    try:
        return (d*w).sum() / w.sum()
    except:
        return np.nan
# %%
vwret = ccm5.groupby(['jdate', 'roeport']).apply(wavg, 'retadj', 'lme').to_frame().reset_index().rename(columns={0: 'vwret'})
# %%
qfactor = vwret.pivot(index='jdate', columns='roeport', values='vwret').reset_index()
# %%
qfactor['R_ROE'] = qfactor['H'] - qfactor['L']
# %%
# Lau's q factor
q5_factor = pd.read_csv('q5_factors_monthly_2021.csv')
q5_factor = q5_factor[q5_factor['year'].astype(str)>=sample_start_date[-4:]].reset_index(drop=True)

# %%
stats.pearsonr(q5_factor.iloc[3:144,:]['R_ROE'], qfactor['R_ROE']*100)
# %%
q5_factor.iloc[3:144,:].reset_index()['R_ROE'].plot()

# %%
(qfactor['R_ROE']*100).plot()
# %%
