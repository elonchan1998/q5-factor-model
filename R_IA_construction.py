# %%
from distutils.errors import CompileError
import pandas as pd
import numpy as np
import datetime as dt
from pyparsing import col
import wrds
import matplotlib.pyplot as plt
# from dateutil.relativedelta import *
from pandas.tseries.offsets import MonthEnd, YearEnd
from scipy import stats

# %%
conn=wrds.Connection()
# %%
sample_start_date = '01/01/2010'
sample_end_date = '12/31/2021'
# %%
# Investment to Assets I/A
# change in total assets divided by one year lagged total assets
comp_ia = conn.raw_sql(f"""
                        select gvkey, datadate, at
                        from comp.funda
                        where indfmt = 'INDL'
                        and datafmt = 'STD'
                        and popsrc='D'
                        and consol='C'
                        and curcd='USD'
                        and datadate between '{sample_start_date}' and '{sample_end_date}'
                        """, date_cols=['datadate'])

comp_ia['year'] = comp_ia['datadate'].dt.year
# %%
comp_ia['chg in at'] = comp_ia.groupby(['gvkey'])['at'].diff()
comp_ia['lat'] = comp_ia.groupby(['gvkey'])['at'].shift()
# %%
comp_ia['IA'] = comp_ia['chg in at']/comp_ia['lat']
comp_ia['count'] = comp_ia.groupby(['gvkey']).cumcount()
# %%

#CRSP
crsp_m = conn.raw_sql(f"""
                       select a.permno, a.permco, a.date, b.siccd, b.shrcd, 
                       b.exchcd, a.ret, a.retx, a.shrout, a.prc
                       from crsp.msf as a
                       left join crsp.msenames as b
                       on a.permno=b.permno
                       and b.namedt <= a.date
                       and a.date <= b.nameendt
                       where a.date between '{sample_start_date}' and '{sample_end_date}'
                       and b.exchcd between 1 and 3
                       """, date_cols=['date'])
# %%
# filter out financial firms sic 6000 to 6999
crsp_m = crsp_m[(crsp_m['siccd']<6000) | (crsp_m['siccd']>6999)]

# crsp date convention is different from compustat, need to line up
crsp_m['jdate'] = crsp_m['date']+MonthEnd(0)
# %%
# adjust for delisting return
dlret = conn.raw_sql("""
                     select permno, dlret, dlstdt
                     from crsp.msedelist
                     """, date_cols=['dlstdt'])

dlret['jdate'] = dlret['dlstdt']+MonthEnd(0)
#%%
crsp = pd.merge(crsp_m, dlret, how='left', on=['permno', 'jdate'])
crsp['dlret']=crsp['dlret'].fillna(0)
crsp['ret']=crsp['ret'].fillna(0)

# return adjustment
crsp['retadj']=(1+crsp['ret'])*(1+crsp['dlret'])-1
# %%
# market equity
crsp['me']=crsp['prc'].abs()*crsp['shrout']
crsp=crsp.drop(['dlret', 'dlstdt', 'prc', 'shrout'], axis=1)
crsp=crsp.sort_values(by=['jdate', 'permco', 'me'])
# %%
# Aggregate all Market Equity from all stocks issued by the same company 

# sum of me across different permno belonging to the same permco at a given date
crsp_summe = crsp.groupby(['jdate', 'permco'])['me'].sum().reset_index()
# largest market cap within a permco and date
crsp_maxme = crsp.groupby(['jdate', 'permco'])['me'].max().reset_index()

# join by jdate and maxme to find the permno that has the maximum me
crsp1 = pd.merge(crsp, crsp_maxme, how='inner', on=['jdate', 'permco', 'me'])
crsp1 = crsp1.drop(['me'], axis=1)

# merge with sum of me to get the aggregate market equity for each company
crsp2 = pd.merge(crsp1, crsp_summe, how='inner', on=['jdate', 'permco'])
crsp2 = crsp2.sort_values(by=['permno', 'jdate']).drop_duplicates()
# %%
crsp2['year'] = crsp2['jdate'].dt.year
crsp2['month'] = crsp2['jdate'].dt.month
# %%
# July to June dates
crsp2['qdate'] = crsp2['jdate']+MonthEnd(-6)
crsp2['qyear'] = crsp2['qdate'].dt.year
crsp2['qmonth'] = crsp2['qdate'].dt.month
# crsp2['1+retx'] = 1+crsp2['retx']
# crsp2=crsp2.sort_values(by=['permno', 'date'])
# %%
# to determine month t portfolio weight need to use month t-1 characteristic
crsp2['lme'] = crsp2.groupby(['permno'])['me'].shift(1)
# %%
# Info as of June
crsp_jun = crsp2[crsp2['month']==6]
crsp_jun=crsp_jun[['permno','date', 'jdate', 'shrcd','exchcd','retadj','me']]
crsp_jun=crsp_jun.sort_values(by=['permno','jdate']).drop_duplicates()
# %%

# CCM

ccm=conn.raw_sql("""
                  select gvkey, lpermno as permno, linktype, linkprim, 
                  linkdt, linkenddt
                  from crsp.ccmxpf_linktable
                  where substr(linktype,1,1)='L'
                  and (linkprim ='C' or linkprim='P')
                  """, date_cols=['linkdt', 'linkenddt'])

# if linkenddt is missing then set to today date
ccm['linkenddt']=ccm['linkenddt'].fillna(pd.to_datetime('today'))
# %%
# ccm merged with comp
ccm1 = pd.merge(comp_ia[['gvkey', 'datadate', 'IA', 'count']], ccm, how='left', on=['gvkey'])


ccm1['yearend'] = ccm1['datadate'] + YearEnd(0)
ccm1['jdate'] = ccm1['yearend'] + MonthEnd(6)

# fiscal year ending in year t
# if the compustat information is available before end of june
# portfolio formation, we use the info, otherwise push the info to next year
# ccm1['jdate'] = np.where(ccm1['datadate'] > ccm1['datadate']+YearEnd(0)-MonthEnd(6), ccm1['datadate']+YearEnd(0) + MonthEnd(6), ccm1['datadate']+YearEnd(0)-MonthEnd(6))

# %%
# set link date bounds
ccm2=ccm1[(ccm1['jdate']>=ccm1['linkdt'])&(ccm1['jdate']<=ccm1['linkenddt'])]
ccm2=ccm2[['gvkey','permno','datadate', 'jdate', 'IA', 'count']]
# %%
# link comp and crsp
ccm_jun = pd.merge(crsp_jun, ccm2, how='inner', on=['permno', 'jdate'])
# %%
nyse = ccm_jun[(ccm_jun['exchcd']==1) & (ccm_jun['me']>0) & (ccm_jun['count']>=1) \
                & ((ccm_jun['shrcd'] == 10) | (ccm_jun['shrcd'] == 11))]
# %%
# I/A Breakdown
nyse_ia = nyse.groupby(['jdate'])['IA'].describe(percentiles=[0.3, 0.7]).reset_index()
nyse_ia = nyse_ia[['jdate', '30%', '70%']].rename(columns={'30%': 'IA30', '70%': 'IA70'})
# %%
ccm1_jun = pd.merge(ccm_jun, nyse_ia, how='left', on=['jdate'])
# %%
def IA_bucket(row):
    if row['IA'] <= row['IA30']:
        value = 'L'
    elif row['IA'] <= row['IA70']:
        value = 'M'
    elif row['IA'] > row['IA70']:
        value = 'H'
    else:
        value =''
    return value
# %%
ccm1_jun['iaport'] = np.where((ccm1_jun['me']>0), ccm1_jun.apply(IA_bucket, axis=1), '')
ccm1_jun['nonmissport'] = np.where((ccm1_jun['iaport']!=''), 1, 0)
ccm1_jun['qyear'] = ccm1_jun['jdate'].dt.year
# %%
crsp3 = crsp2[['date', 'permno', 'shrcd', 'exchcd', 'retadj', 'me', 'lme', 'qyear', 'jdate']]
# %%
ccm3 = crsp3.merge(ccm1_jun[['permno', 'qyear', 'iaport', 'nonmissport']], how='left', on=['permno', 'qyear'])

# %%
# keeping only records that meet the criteria
ccm4=ccm3[(ccm3['lme']>0) & (ccm3['nonmissport']==1) & 
          ((ccm3['shrcd']==10) | (ccm3['shrcd']==11))]
# %%
def wavg(group, retadj, weight):
    d = group[retadj]
    w = group[weight]
    try:
        return (d*w).sum() / w.sum()
    except:
        return np.nan
# %%

vwret = ccm4.groupby(['jdate', 'iaport']).apply(wavg, 'retadj', 'lme').to_frame().reset_index().rename(columns={0: 'vwret'})
# %%
qfactor = vwret.pivot(index='jdate', columns='iaport', values='vwret').reset_index()
# %%
qfactor['R_IA'] = qfactor['L'] - qfactor['H']
# %%
# Lau's q factor
q5_factor = pd.read_csv('q5_factors_monthly_2021.csv')
q5_factor = q5_factor[q5_factor['year'].astype(str)>=sample_start_date[-4:]].reset_index(drop=True)

# %%
stats.pearsonr(q5_factor.iloc[30:144,:]['R_IA'], qfactor['R_IA']*100)
# %%
q5_factor.iloc[18:144,:].reset_index()['R_IA'].plot()

# %%
(qfactor['R_IA']*100).plot()
# %%
