# %%
import pandas as pd
import numpy as np
import datetime as dt
import wrds
import matplotlib.pyplot as plt
from pandas.tseries.offsets import MonthEnd, YearEnd
from scipy import stats
# %%
conn=wrds.Connection()
#%%
sample_start_date = '01/01/1972'
sample_end_date = '12/31/2021'


#%%
# Lau's q factor
q5_factor = pd.read_csv('q5_factors_monthly_2021.csv')
q5_factor = q5_factor[q5_factor['year'].astype(str)>=sample_start_date[-4:]].reset_index(drop=True)
# %%
# 1m treasury rate
treasury = conn.raw_sql(f"""
                        select kytreasnox, mcaldt, tmyld
                        from crsp.tfz_mth_rf2
                        where mcaldt between '{sample_start_date}' and '{sample_end_date}'
                        and kytreasnox = 2000061
                        """, date_cols=['mcaldt'])
# %%
treasury.to_csv('q_treasury.csv')
#%%
# CRSP
# keep stock with CRSP share code 10 and 11
crsp_m = conn.raw_sql(f"""
                       select a.permno, a.permco, a.date, b.siccd, 
                       b.exchcd, a.ret, a.retx, a.shrout, a.prc
                       from crsp.msf as a
                       left join crsp.msenames as b
                       on a.permno=b.permno
                       and b.namedt <= a.date
                       and a.date <= b.nameendt
                       and b.shrcd in (10, 11)
                       where a.date between '{sample_start_date}' and '{sample_end_date}'
                       and b.exchcd between 1 and 3
                       """, date_cols=['date'])
#%%

crsp_m.to_csv('q_crsp_m.csv')

#%%
crsp_m['jdate'] = crsp_m['date'] + MonthEnd(0)

# %%
# adjust for delisting return
dlret = conn.raw_sql("""
                     select permno, dlret, dlstdt
                     from crsp.msedelist
                     """, date_cols=['dlstdt'])

dlret['jdate'] = dlret['dlstdt']+MonthEnd(0)

crsp = pd.merge(crsp_m, dlret, how='left', on=['permno', 'jdate'])
crsp['dlret']=crsp['dlret'].fillna(0)
crsp['ret']=crsp['ret'].fillna(0)

# return adjustment
crsp['retadj']=(1+crsp['ret'])*(1+crsp['dlret'])-1

#%%
# Compute the Market Equity and it's lagged value

crsp['me'] = crsp['prc'].abs() * crsp['shrout']
crsp=crsp.drop(['dlret', 'dlstdt', 'prc', 'shrout'], axis=1)

#%%
# Aggregate all Market Equity from all stocks issued by the same company 

# sum of me across different permno belonging to the same permco at a given date
crsp_summe = crsp.groupby(['jdate', 'permco'])['me'].sum().reset_index()
# largest market cap within a permco and date
crsp_maxme = crsp.groupby(['jdate', 'permco'])['me'].max().reset_index()

# join by jdate and maxme to find the permno that has the maximum me
crsp1 = pd.merge(crsp, crsp_maxme, how='inner', on=['jdate', 'permco', 'me'])
crsp1 = crsp1.drop(['me'], axis=1)

# merge with sum of me to get the aggregate market equity for each company
crsp2 = pd.merge(crsp1, crsp_summe, how='inner', on=['jdate', 'permco'])


# we computed the market equity for the corresponding company of the permno,
# we keep both permno and permco because we need permco to 
# identify company wise information and permno for return computation
# hence and lme is grouped by permno as lme will be used as the weight
# for calculating the portfolio return
crsp2['lme'] = crsp2.groupby(['permno'])['me'].shift(1)
#%%
# Constructing the Market Factor

# value weight return
def wavg(group, retadj, weight):
    d = group[retadj]
    w = group[weight]
    try:
        return (d*w).sum() / w.sum()
    except:
        return np.nan
# %%
vwret_mkt = crsp2.groupby(['jdate']).apply(wavg, 'retadj', 'lme').to_frame().reset_index().rename(columns={0: 'vwret_mkt'})
# %%
R_MKT = vwret_mkt['vwret_mkt']*100 - treasury['tmyld']*100
# %%
# correlation check
stats.pearsonr(q5_factor['R_MKT'], R_MKT)
# %%
# for market factor we don't exclude financial firms and firms with negative be

# Exclude financial firms from the data
crsp3 = crsp2[(crsp2['siccd']<6000) | (crsp2['siccd']>6999)]

crsp3 = crsp3.drop(['siccd'], axis=1)

#%%
# for January to June in year t+1, their portfolio classification are
# determined in June year t, we define July at year t to June year t+1
# as a qyear. So we subtract 6 months from calender year to determine 
# their qyear
# July to June dates
crsp3['qdate'] = crsp3['jdate']+MonthEnd(-6)
crsp3['qyear'] = crsp3['qdate'].dt.year

#%%
# Exclude firms with negative book value

# this comp is for compute be and ROE
comp = conn.raw_sql(f"""
                     select gvkey, datadate, txditcq, seqq, ceqq, pstkq, atq, ltq, pstkrq,
                     ibq, rdq
                     from comp.fundq
                     where indfmt = 'INDL'
                     and datafmt = 'STD'
                     and popsrc='D'
                     and consol='C'
                     and curcdq='USD'
                     and datadate between '{sample_start_date}' and '{sample_end_date}'
                     """, date_cols=['datadate'])
#%%
comp.to_csv('q_comp.csv')
#%%
# some rows are duplicated and may contain different values, need to drop
comp = comp[~comp[['gvkey', 'datadate']].duplicated()]

#%%
# compute the book equity

# define shareholder's equity by this order
comp['se'] = comp['seqq']
comp['se'] = np.where(comp['se'].isnull(), comp['ceqq']+comp['pstkq'], comp['se'])
comp['se'] = np.where(comp['se'].isnull(), comp['atq']-comp['ltq'], comp['se'])

comp['be'] = comp['se'] + comp['txditcq'] - comp['pstkrq']

# 1 quarter lagged be
comp['lbe'] = comp.groupby(['gvkey'])['be'].shift()

# define ROE
comp['ROE'] = comp['ibq'] / comp['lbe']

#%%
comp2 = comp.drop(['seqq', 'ceqq', 'pstkq', 'atq', 'ltq', 'se', 'txditcq', 'pstkrq', 'ibq'], axis=1)
#%%

# this comp is for IA in annual frequency
comp_ia = conn.raw_sql(f"""
                        select gvkey, datadate, at
                        from comp.funda
                        where indfmt = 'INDL'
                        and datafmt = 'STD'
                        and popsrc='D'
                        and consol='C'
                        and curcd='USD'
                        and datadate between '{sample_start_date}' and '{sample_end_date}'
                        """, date_cols=['datadate'])

# there is no duplication in comp_ia
#%%
comp_ia.to_csv('q_comp_ia.csv')

#%%
comp_ia['chg in at'] = comp_ia.groupby(['gvkey'])['at'].diff()
comp_ia['lat'] = comp_ia.groupby(['gvkey'])['at'].shift()

comp_ia['IA'] = comp_ia['chg in at']/comp_ia['lat']

#%%
comp3 = pd.merge(comp2, comp_ia[['gvkey', 'datadate', 'IA']], how='left', on=['gvkey', 'datadate'] )

#%%
# sort by gvkey and datadate
# create jdate to match date when join
comp3['jdate'] = comp3['datadate'] + MonthEnd(0)
comp3 = comp3.sort_values(['gvkey', 'jdate'])

#%%
# Use CCM to link compustat and CRSP
ccm=conn.raw_sql("""
                  select gvkey, lpermno as permno, linktype, linkprim, 
                  linkdt, linkenddt
                  from crsp.ccmxpf_linktable
                  where substr(linktype,1,1)='L'
                  and (linkprim ='C' or linkprim='P')
                  """, date_cols=['linkdt', 'linkenddt'])

# is the link is still valid, it will be null. so need replace to avoid filter
ccm['linkenddt']=ccm['linkenddt'].fillna(pd.to_datetime('today'))
#%%
# ccm merged with comp
ccm1 = pd.merge(comp3, ccm, how='left', on=['gvkey'])

# ccm1['yearend'] = ccm1['datadate'] + YearEnd(0)
# ccm1['jdate'] = ccm1['yearend'] + MonthEnd(6)

# fiscal year ending in year t
# if the compustat information is available before end of june
# portfolio formation, we use the info, otherwise push the info to next year
# ccm1['jdate'] = np.where(ccm1['datadate'] > ccm1['datadate']+YearEnd(0)-MonthEnd(6), ccm1['datadate']+YearEnd(0) + MonthEnd(6), ccm1['datadate']+YearEnd(0)-MonthEnd(6))

#%%
# keep record which exist within link date bounds
ccm2 = ccm1[(ccm1['jdate']>=ccm1['linkdt'])&(ccm1['jdate']<=ccm1['linkenddt'])]
ccm2 = ccm2[['gvkey', 'permno', 'jdate', 'rdq', 'be', 'lbe', 'IA', 'ROE']]

#%%

# link comp and crsp
ccm3 = pd.merge(crsp3, ccm2, how='left', on=['permno', 'jdate'])



#%%
# ffill 
ccm4 = ccm3.copy(deep=True)
ccm4[['be', 'lbe', 'IA', 'ROE']] = ccm4[['be', 'lbe', 'IA', 'ROE']].ffill()

#%%
# Info as of June
ccm_jun = ccm4[ccm4['jdate'].dt.month == 6]

#%%
# nyse breakpoint for sz and IA
nyse = ccm_jun[(ccm_jun['exchcd']==1) & (ccm_jun['lme']>0) & (ccm_jun['lbe']>0)]

nyse_sz = nyse.groupby(['jdate'])['me'].median().to_frame().reset_index().rename(columns={'me':'sizemedn'})

nyse_ia = nyse.groupby(['jdate'])['IA'].describe(percentiles=[0.3, 0.7]).reset_index()
nyse_ia = nyse_ia[['jdate', '30%', '70%']].rename(columns={'30%': 'IA30', '70%': 'IA70'})

nyse_breaks = pd.merge(nyse_sz, nyse_ia, how='inner', on=['jdate'])

#%%
ccm1_jun = pd.merge(ccm_jun, nyse_breaks, how='left', on=['jdate'])
#%%
def sz_bucket(row):
    if row['me'] == np.nan:
        value = ''
    elif row['me'] <= row['sizemedn']:
        value = 'S'
    elif row['me'] > row['sizemedn']:
        value = 'B'
    return value

def IA_bucket(row):
    if row['IA'] <= row['IA30']:
        value = 'L'
    elif row['IA'] <= row['IA70']:
        value = 'M'
    elif row['IA'] > row['IA70']:
        value = 'H'
    else:
        value =''
    return value
#%%
ccm1_jun['szport'] = np.where((ccm1_jun['me'] > 0), ccm1_jun.apply(sz_bucket, axis=1), '')
ccm1_jun['iaport'] = np.where((ccm1_jun['me'] > 0), ccm1_jun.apply(IA_bucket, axis=1), '')

ccm1_jun['nonmissport'] = np.where((ccm1_jun['szport']!='') & (ccm1_jun['iaport']!=''), 1, 0)
ccm1_jun['qyear'] = ccm1_jun['jdate'].dt.year

#%%
ccm5 = pd.merge(ccm4, ccm1_jun[['permno', 'szport', 'iaport', 'nonmissport', 'qyear']], how='left', on=['permno', 'qyear'])
# keeping only records that meet the criteria
# lme > 0 as we use month t-1 characteristic to determine month t weight
ccm5 = ccm5[(ccm5['lbe'] > 0) & (ccm5['lme']>0) & (ccm5['nonmissport']==1)]

#%%
# nyse break point for ROE
nyse2 = ccm5[(ccm5['exchcd']==1) & (ccm5['lme']>0) & (ccm5['lbe']>0)]

nyse_roe = nyse2.groupby(['jdate'])['ROE'].describe(percentiles=[0.3, 0.7]).reset_index()
nyse_roe = nyse_roe[['jdate', '30%', '70%']].rename(columns={'30%': 'ROE30', '70%': 'ROE70'})

#%%
ccm6 = pd.merge(ccm5, nyse_roe, how='left', on=['jdate'])

#%%
def ROE_bucket(row):
    if row['ROE'] <= row['ROE30']:
        value = 'L'
    elif row['ROE'] <= row['ROE70']:
        value = 'M'
    elif row['ROE'] > row['ROE70']:
        value = 'H'
    else:
        value =''
    return value
#%%
ccm6['roeport'] = np.where((ccm6['me']>0), ccm6.apply(ROE_bucket, axis=1), '')
ccm6['nonmissport'] = np.where((ccm6['nonmissport']==1) & (ccm6['roeport']!=''), 1, 0)

ccm6 = ccm6[(ccm6['lbe'] > 0) & (ccm6['lme']>0) & (ccm6['nonmissport']==1)]

#%%
vwret = ccm6.groupby(['jdate','szport','iaport', 'roeport']).apply(wavg, 'retadj','lme').to_frame().reset_index().rename(columns={0: 'vwret'})
vwret['port'] =  vwret['szport'] + vwret['iaport'] + vwret['roeport']

#%%
q_factors = vwret.pivot(index='jdate', columns='port', values='vwret').reset_index()
#%%
q_factors['R_ME'] = q_factors.iloc[:, q_factors.columns.str[0]=='S'].mean(axis=1) - q_factors.iloc[:, q_factors.columns.str[0]=='B'].mean(axis=1)
q_factors['R_IA'] = q_factors.iloc[:, q_factors.columns.str[1]=='L'].mean(axis=1) - q_factors.iloc[:, q_factors.columns.str[1]=='H'].mean(axis=1)
q_factors['R_ROE'] = q_factors.iloc[:, q_factors.columns.str[2]=='H'].mean(axis=1) - q_factors.iloc[:, q_factors.columns.str[2]=='L'].mean(axis=1)

#%%
#multiply by 100 to change the factors to percentage
q_factors.iloc[:, -3:] = q_factors.iloc[:, -3:]*100

# %%
# things to do
# 6 months condition

#%%
q5_factor = q5_factor.loc[6:, :]
q5_factor.index = q_factors['jdate']

q_factors.index = q_factors['jdate']

q_factors['R_MKT'] = R_MKT[6:].values
#%%
plt.figure(figsize=(16,12))
plt.suptitle('Comparison of Results', fontsize=20)

ax1 = plt.subplot(411)
ax1.set_title('R_MKT', fontsize=15)
ax1.set_xlim([dt.datetime(1972,7,1), dt.datetime(2021,12,31)])
ax1.plot(q5_factor['R_MKT'], 'r--', q_factors['R_MKT'], 'b-')
ax1.legend(('q5R_MKT','Replicated R_MKT'), loc='upper right', shadow=True)

ax2 = plt.subplot(412)
ax2.set_title('R_ME', fontsize=15)
ax2.set_xlim([dt.datetime(1972,7,1), dt.datetime(2021,12,31)])
ax2.plot(q5_factor['R_ME'], 'r--', q_factors['R_ME'], 'b-')
ax2.legend(('q5R_ME','Replicated R_ME'), loc='upper right', shadow=True)

ax3 = plt.subplot(413)
ax3.set_title('R_IA', fontsize=15)
ax3.plot(q5_factor['R_IA'], 'r--', q_factors['R_IA'], 'b-')
ax3.set_xlim([dt.datetime(1972,7,1), dt.datetime(2021,12,31)])
ax3.legend(('q5R_IA','Replicated R_IA'), loc='upper right', shadow=True)

ax4 = plt.subplot(414)
ax4.set_title('R_ROE', fontsize=15)
ax4.plot(q5_factor['R_ROE'], 'r--', q_factors['R_ROE'], 'b-')
ax4.set_xlim([dt.datetime(1972,7,1), dt.datetime(2021,12,31)])
ax4.legend(('q5R_ROE','Replicated R_ROE'), loc='upper right', shadow=True)

plt.subplots_adjust(top=0.92, hspace=0.2)
# %%
#%%
print(stats.pearsonr(q5_factor['R_MKT'], q_factors['R_MKT']))
print(stats.pearsonr(q5_factor['R_ME'], q_factors['R_ME']))
print(stats.pearsonr(q5_factor['R_IA'], q_factors['R_IA']))
print(stats.pearsonr(q5_factor['R_ROE'], q_factors['R_ROE']))
# %%
from statsmodels.tsa.stattools import adfuller
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf

from statsmodels.tsa.ar_model import AutoReg
# %%
adfuller(q5_factor['R_MKT'], autolag='BIC')
adfuller(q5_factor['R_ME'], autolag='BIC')
adfuller(q5_factor['R_IA'], autolag='BIC')
adfuller(q5_factor['R_ROE'], autolag='BIC')
# %%
plot_acf(q5_factor['R_MKT'], lags=25)
plot_acf(q5_factor['R_ME'], lags=25)
plot_acf(q5_factor['R_IA'], lags=25)
plot_acf(q5_factor['R_ROE'], lags=25)
# %%
ar_model = AutoReg(q5_factor['R_ROE'], lags=8).fit()
ar_model.summary()
# %%
from arch import arch_model
# %%
garch = arch_model(q5_factor['R_ROE'], vol='GARCH', p=1, q=1)
garch_fit = garch.fit()
garch_fit
# %%
corr = pd.merge(q5_factor.iloc[:, 4:7], q_factors[['R_MKT', 'R_ME', 'R_IA', 'R_ROE']], how='inner', on=['jdate'])
# %%
from statsmodels.tsa.api import VAR
# %%
model = VAR(q_factors[['R_MKT', 'R_ME', 'R_IA', 'R_ROE']])
# %%
x = model.select_order(maxlags=12)
x.summary()
# %%
model_fitted = model.fit(1)
model_fitted.summary()
# %%
